<div class="container">
	<main>
		<div style="text-align: center">
			<h1>500</h1>
			<p><?php echo $this->getArg('error')->getMessage() ?: 'Le serveur n\'a pas pu traiter votre demande.'; ?></p>
		</div>
	</main>
</div>
