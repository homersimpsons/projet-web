<div class="container">
	<main>
		<div style="text-align: center">
			<h1>404</h1>
			<p><?php echo $this->getArg('error')->getMessage() ?: 'Cette page n\'existe pas'; ?></p>
		</div>
	</main>
</div>
