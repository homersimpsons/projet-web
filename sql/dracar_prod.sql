-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 19, 2018 at 10:52 PM
-- Server version: 5.7.21-0ubuntu0.17.10.1
-- PHP Version: 7.1.15-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dracar`
--

--
-- Dumping data for table `IP`
--

INSERT INTO `IP` (`ID_IP`, `IP`, `IP_BANNI`) VALUES
(1, '127.0.0.1', 0);

--
-- Dumping data for table `LOG_CONNECTION`
--

INSERT INTO `LOG_CONNECTION` (`ID_LOG_CONNECTION`, `ID_IP`, `DATE`) VALUES
(1, 1, '2018-04-19 22:42:23'),

--
-- Dumping data for table `USER`
--

INSERT INTO `USER` (`ID_USER`, `ID_LOG_CONNECTION`, `NOM`, `PRENOM`, `EMAIL`, `TELEPHONE`, `PASSWORD`, `ADMIN`, `PSEUDO`, `BANNI`) VALUES
(1, 1, 'Admin', 'Admin', 'admin@example.com', '0612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'admin', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
